package org.craftmanship.money

import spock.lang.Specification
import spock.lang.Unroll

import static org.craftmanship.money.BigDecimalMoney.of


class MoneySpec extends Specification {

    @Unroll
    def "should #operand1 + #operand2 = #expected"() {
        given: "Two quantities of money"
        Money oneMoney = of(operand1)
        Money anotherMoney = of(operand2)

        when: "Add one to the other"
        def result = oneMoney.add(anotherMoney)

        then: "Result is the two quantities added"
        result.toString() == expected

        where:
        operand1    | operand2    | expected
        "453.85EUR" | "512.96EUR" | "966.81EUR"
        "0.25EUR"   | "0.75EUR"   | "1.00EUR"
        "0.00EUR"   | "0.00EUR"   | "0.0EUR"
        "1.50EUR"   | "0.00EUR"   | "1.5EUR"
        "0.00EUR"   | "1.50EUR"   | "1.5EUR"
    }

}